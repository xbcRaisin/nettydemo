package com.example.demo.model.heart;

import java.io.Serializable;
import lombok.Data;

/**
 * @ClassName: Heart
 * @Author: huangzf
 * @Date: 2018/8/6 15:26
 * @Description: 心跳
 */
@Data
public class Heart implements Serializable {

    private static final long serialVersionUID = -7033707301911915195L;

    private String heart;

}
